
CC ?= gcc
LOLCAT_SRC ?= lolcat.c
CFLAGS ?= -std=c11 -Wall -Wextra -g -DPASE -D_XOPEN_SOURCE_EXTENDED=1

all: lolcat

.PHONY: clean

lolcat: $(LOLCAT_SRC)
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -f lolcat